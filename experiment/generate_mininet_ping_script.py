
import sys
import random


if(len(sys.argv) != 3):
    print("Not enough arguments")
    exit

mn_topology_type = sys.argv[1]
n_hosts = int(sys.argv[2])


f = open("{}.mn".format(mn_topology_type), "w")

f.write("sh sleep 10\n")

chosen_host=random.randint(1,n_hosts)

for i in range(0,n_hosts+1):
    if(i != chosen_host):
        f.write("h{} ping h{} -c 1 -U\n".format(str(chosen_host), str(i)))

f.close()
